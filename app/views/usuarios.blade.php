@extends('layout')

@section('content')

	<form role="form" class="form-signin">
		<h3>Bienvenido <?php echo $nombre; ?></h3>
		<br>
		<ul>
			<?php foreach ($usuarios as $user): ?>
				<li><?php echo $user->email; ?></li>
			<?php endforeach ?>
		</ul>

		<a href="logout" class="btn btn-lg btn-primary btn-block">Salir</a>

	</form>

@stop