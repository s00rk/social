@extends('layout')

@section('content')

	{{ Form::open(array('action' => 'HomeController@login', 'class'=>'form-signin', 'role'=>'form')) }}
		<h2 class="form-signin-heading">Login</h2>
		<input name="email" type="email" class="form-control" placeholder="Email" required autofocus>
		<input name="password" type="password" class="form-control" placeholder="Contraseña" required>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
		<a href="registro" class="btn btn-lg btn-success btn-block">Registro</a>

		@if (Session::has('error'))
			<h4><span class="label label-danger">{{ Session::get('error') }}</span></h4>
		@endif
	{{ Form::close() }}

@stop