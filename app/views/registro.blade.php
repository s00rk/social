@extends('layout')

@section('content')

	{{ Form::open(array('action' => 'HomeController@registro_post', 'class'=>'form-signin', 'role'=>'form')) }}
		<h2 class="form-signin-heading">Registro</h2>

		<input name="nombre" type="text" class="form-control" placeholder="Nombre" required autofocus>
		<input name="apellido" type="text" class="form-control" placeholder="Apellido" required>
		<input name="fecha_nacimiento" type="date" class="form-control" placeholder="Fecha Nacimiento" required>

		<input name="email" type="email" class="form-control" placeholder="Email" required>
		<input name="password" type="password" class="form-control" placeholder="Contraseña" required>
		<input name="r_password" type="password" class="form-control" placeholder="Repetir Contraseña" required>
		<br>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Registrarse</button>

		@if (Session::has('error'))
			<h4><span class="label label-danger">{{ Session::get('error') }}</span></h4>
		@endif
	{{ Form::close() }}

@stop