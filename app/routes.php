<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login', array('as' => 'login', 'uses' => 'HomeController@showWelcome'));
Route::post('login', 'HomeController@login');
Route::get('logout', array('as' => 'logout', 'uses' => 'HomeController@logout'));

Route::get('registro', 'HomeController@registro');
Route::post('registro', 'HomeController@registro_post');

Route::group(array('before' => 'auth'), function()
{
	Route::get('/', 'HomeController@showUsuarios');
});