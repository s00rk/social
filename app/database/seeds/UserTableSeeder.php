<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array('email' => 'victor@social.com', 'password' => Hash::make('victor_pass') ));
    }

}