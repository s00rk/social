<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($t){
			$t->increments('id');
			$t->string('username', 16)->unique()->nullable();
			$t->string('password', 60);
			$t->string('nombre', 50)->nullable();
			$t->string('apellido', 50)->nullable();
			$t->string('email')->unique();
			$t->date('f_nacimiento')->nullable();
			$t->string('remember_token', 100)->nullable();
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
