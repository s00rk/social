<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('home');
	}

	public function showUsuarios()
	{
		$usuarios = User::all();
		$nombre = Auth::user()->nombre;
		return View::make('usuarios', array('usuarios' => $usuarios, 'nombre' => $nombre));
	}

	public function login()
	{
		$user = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);

		if (Auth::attempt($user)) {
			return Redirect::action('HomeController@showUsuarios');
		}
		return Redirect::action('HomeController@showWelcome')->with('error', 'Usuario/Contraseña incorrecta.');
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::route('login');
	}

	public function registro()
	{
		return View::make('registro');
	}

	public function registro_post()
	{
		if(Input::get('password') != Input::get('r_password'))
		{
			return Redirect::action('HomeController@registro')->with('error', 'Contraseña no coincide!');
		}


		$user = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'nombre' => Input::get('nombre'),
			'apellido' => Input::get('apellido'),
			'f_nacimiento' => Input::get('fecha_nacimiento')
		);

		$rules = array(
			'email' => 'required|email|unique:users',
			'password' => 'required|min:8',
			'nombre' => 'required',
			'apellido' => 'required',
			'f_nacimiento' => 'required'
		);

		$validator = Validator::make($user, $rules);

		if ($validator->fails())
		{
			return Redirect::action('HomeController@registro')->with('error', $validator->messages());
		}

		$user['password'] = Hash::make($user['password']);
		print_r($user);
		
		User::create($user);
		Auth::attempt(array('email' => $user['email'], 'password' => Input::get('password')));
		return Redirect::action('HomeController@showUsuarios');

	}


}
